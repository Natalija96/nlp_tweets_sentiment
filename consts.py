NUM_SAMPLES = 15000
COLUMN_X_NAME, COLUMN_Y_NAME = 'Text', 'Target'
DATA_PATH = "./dataset/data.csv"
SUB_DATA_PATH = "./dataset/subdata3.csv"
TRAIN = "./dataset/train.csv"
POSITIVE_WORD = 'pos'
NEGATIVE_WORD = 'neg'