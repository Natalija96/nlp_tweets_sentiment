from sklearn.model_selection import train_test_split
import generate_features as pp


def process_data(data, y):
    all_words = pp.get_all_words(data)

    x_train, x_test, y_train, y_test = train_test_split(data, y, test_size=0.2, random_state=55)

    x_train_vec = pp.generate_feature_vectors(x_train, all_words)
    x_test_vec = pp.generate_feature_vectors(x_test, all_words)

    train_set = pp.generate_features_sets(x_train_vec, y_train)
    test_set = pp.generate_features_sets(x_test_vec, y_test)

    return x_train, x_test, y_train, y_test, x_train_vec, x_test_vec, train_set, test_set