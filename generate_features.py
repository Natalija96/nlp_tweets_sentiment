import nltk
import clean_data as pp


def get_all_words(data):
    tokens = []
    for index, row in data.iterrows():
        tokenized_row = pp.tokenize(row[0])
        for word in tokenized_row:
            tokens.append(word)

    print("LEN:" + str(len(tokens)))
    tokens = nltk.FreqDist(tokens)
    tokens = list(tokens.keys())[:3000]

    return tokens


def find_features(row, all_words):
    """
    :param row: data frame row
    :param all_words: al
    :return: row feature in format : [{'word1':True}..]
    """
    tokenized_row = pp.tokenize(row)
    words = set(tokenized_row)
    features = {}
    for word in all_words:
        features[word] = (word in words)

    return features


def generate_features_sets(x, y):
    """
    Generates feature sets for vector x and y
    :param x: dictionary of features
    :param y: positive or negative label vector
    :return:
    """
    feature_sets = []
    for index in range(len(x)):
        feature_set = x[index]
        feature_sets.append((feature_set, y[index]))
    return feature_sets


def generate_feature_vectors(data, all_words):
    feature_vectors = []
    for index, row in data.iterrows():
        feature_vector = find_features(row[0], all_words)
        feature_vectors.append(feature_vector)

    return feature_vectors



