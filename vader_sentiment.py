from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk import sent_tokenize


def vader_classifier(x_test):
    predicted_vader = []
    for index, sentence in x_test.iterrows():

        sid = SentimentIntensityAnalyzer()
        sentences = sent_tokenize(sentence[0])
        score = 0

        for sent in sentences:
            score_dict = sid.polarity_scores(sent)
            score = score + score_dict['compound']

        score = score / len(sentences);
        if score > 0:
            predicted_vader.append(4)
        else:
            [predicted_vader.append(0)]

    return predicted_vader
