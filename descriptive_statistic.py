import matplotlib.pyplot as plt
import seaborn as sns


def plot_count(data, data_column, file_to_save):
    plt.figure(figsize=(14, 8))
    sns.countplot(x=data_column, data=data)
    plt.savefig(file_to_save)
    plt.show()


def confusion_matrix(conf_matrix, file_to_save):
    fig, ax = plt.subplots(figsize=(7.5, 7.5))
    ax.matshow(conf_matrix, cmap=plt.cm.Blues, alpha=0.3)

    for i in range(conf_matrix.shape[0]):
        for j in range(conf_matrix.shape[1]):
            ax.text(x=j, y=i, s=conf_matrix[i, j], va='center', ha='center', size='xx-large')

    alpha = ['neg', 'pos']
    plt.xlabel('Actuals', fontsize=18)
    plt.ylabel('Predictions', fontsize=18)
    ax.set_xticklabels([''] + alpha)
    ax.set_yticklabels([''] + alpha)
    plt.title('Confusion Matrix', fontsize=18)
    plt.savefig(file_to_save)
    plt.show()