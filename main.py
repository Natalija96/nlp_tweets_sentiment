import consts
import nltk.classify
import pandas as pd
from sklearn.metrics import confusion_matrix, classification_report
import vader_sentiment as vs
import split_data as sd
import  descriptive_statistic as ds

tweets = pd.read_csv(consts.SUB_DATA_PATH)
#tweets = shuffle(tweets)
#tweets = tweets.sample(consts.NUM_SAMPLES)
data = tweets[[consts.COLUMN_X_NAME, consts.COLUMN_Y_NAME]]

x = data[consts.COLUMN_X_NAME].values
y = data[consts.COLUMN_Y_NAME].values

###########################################

x_train, x_test, y_train, y_test, x_train_vec, x_test_vec, train_set, test_set = sd.process_data(data, y)
classifier = nltk.NaiveBayesClassifier.train(train_set)

print("\n***Trained NaiveBayes:***\n")
predicted = classifier.classify_many(x_test_vec)
conf_matrix = confusion_matrix(predicted, y_test)
print(conf_matrix)

ds.plot_count(data, "Target", "Count_Plot.jpg")
ds.confusion_matrix(conf_matrix, "Confusion_Matrix_NaiveBayes.jpg")

print(classification_report(y_true=y_test, y_pred=predicted))
classifier.show_most_informative_features(20)

print("\n***Vader:***\n")

predicted_vader = vs.vader_classifier(x_test)
conf_matrix = confusion_matrix(predicted_vader, y_test)
print(conf_matrix)

ds.confusion_matrix(conf_matrix, "Confusion_Matrix_Vader.jpg")

print(classification_report(y_true=y_test, y_pred=predicted_vader))

