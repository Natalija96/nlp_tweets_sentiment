import string
import nltk
from nltk import word_tokenize
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
import re

emojis = {':)': 'smile', ':-)': 'smile', ';d': 'wink', ':-E': 'vampire', ':(': 'sad',':-(': 'sad', ':-<': 'sad', ':P': 'raspberry', ':O': 'surprised',
          ':-@': 'shocked', ':@': 'shocked',':-$': 'confused', ':\\': 'annoyed',':#': 'mute', ':X': 'mute', ':^)': 'smile', ':-&': 'confused',
          '$_$': 'greedy','@@': 'eyeroll', ':-!': 'confused', ':-D': 'smile', ':-0': 'yell', 'O.o': 'confused','<(-_-)>': 'robot', 'd[-_-]b': 'dj',
          ":'-)": 'sadsmile',';)': 'wink',';-)': 'wink', 'O:-)': 'angel','O*-)': 'angel','(:-D': 'gossip', '=^.^=': 'cat'}

def letters(row):
    valids = []
    for character in row:
        if character.isalpha() or character is ' ':
            valids.append(character)
    return ''.join(valids)


def remove_stopwords(tokens):
    """

    :param tokens:
    :return:
    """
    stop_words = stopwords.words('english')
    filtered_tokens = []
    for word in tokens:
        if word not in stop_words and len(word) > 1:
            filtered_tokens.append(word)

    return filtered_tokens


def to_lower(tokens):
    lower = []
    for token in tokens:
        token = token.lower()
        lower.append(token)

    return lower


def lemmatization(tokens):
    lemmatizer = WordNetLemmatizer()
    lemmatized_words = []
    for word in tokens:
        lemmatized_word = lemmatizer.lemmatize(word)
        lemmatized_words.append(lemmatized_word)
    return lemmatized_words


def remove_punctuation(data):
    clean_data = "".join([char for char in data if char not in string.punctuation])
    return clean_data


def remove_numbers(data):
    clean_data = "".join([char for char in data if char not in string.digits])
    return clean_data


# Function to clean the data.
def preprocess(tweet):
    # Defining regex patterns.
    urlPattern = r"((http://)[^ ]*|(https://)[^ ]*|( www\.)[^ ]*)"
    userPattern = '@[^\s]+'
    alphaPattern = "[^a-zA-Z0-9]"
    sequencePattern = r"(.)\1\1+"
    seqReplacePattern = r"\1\1"

    tweet = tweet.lower()
    # Replace all URls with 'URL'
    tweet = re.sub(urlPattern, ' URL', tweet)
    # Replace all emojis.
    for emoji in emojis.keys():
        tweet = tweet.replace(emoji, "EMOJI" + emojis[emoji])
        #print("EMOJI")
        # Replace @USERNAME to 'USER'.
   # tweet = re.sub(userPattern, ' USER', tweet)
    # Replace all non alphabets.
    tweet = re.sub(alphaPattern, " ", tweet)
    # Replace 3 or more consecutive letters by 2 letter.
    #tweet = re.sub(sequencePattern, seqReplacePattern, tweet)

    return tweet


def remove_punctuation_tokens(tokens):
    new_tokens=[]
    for token in tokens:
        p = False
        for char in token:
            if char in string.punctuation or char is '/':
                p = True
        if not p:
            new_tokens.append(token)

    return new_tokens


def remove_numbers_tokens(tokens):
    new_tokens = []
    for token in tokens:
        p = False
        for char in token:
            if char in string.digits:
                p = True
        if not p:
            new_tokens.append(token)

    return new_tokens

def speller_tokens(tokens, speller):

    new_tokens = []
    for token in tokens:
        token = speller(token)
        new_tokens.append(token)

    return new_tokens;


def tokenize(data):
    data = remove_numbers(data)
    data = preprocess(data)
    #data = remove_punctuation(data)
    #data =letters(data)
    tokens = word_tokenize(data)
   # tokens = remove_numbers_tokens(tokens)
    #tokens = remove_punctuation_tokens(tokens)
    tokens = to_lower(tokens)
    tokens = lemmatization(tokens)
    tokens = stemming(tokens)
    #tokens = speller_tokens(tokens, speller)

    tokens = remove_stopwords(tokens)
    tokens = remove_singe_letter_words(tokens)
    return tokens


def stemming(tokens):
    stemmer_ps = PorterStemmer()
    stem_tokens = []
    for word in tokens:
        stem = stemmer_ps.stem(word)
        stem_tokens.append(stem)

    return stem_tokens


def remove_singe_letter_words(tokens):
    new_tokens = []
    for token in tokens:
        if len(token) > 1:
            new_tokens.append(token)

    return new_tokens


def clean_row(row):
    row = letters(row)
    row = remove_singe_letter_words(row)
    return row




